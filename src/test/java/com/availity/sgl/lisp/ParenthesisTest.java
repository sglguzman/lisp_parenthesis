package com.availity.sgl.lisp;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ParenthesisTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testNoStrings1() {
		String s = "(defun csg-intersection-intersect-all (obj-a obj-b)"
   +"(lambda (ray)"		   
   +" (flet ((inside-p (obj) (lambda (d) (inside-p obj (ray-point ray d)))))"
   +"    (merge 'fvector"
   +"           (remove-if-not (inside-p obj-b) (intersect-all obj-a ray))"
   +"           (remove-if-not (inside-p obj-a) (intersect-all obj-b ray))"
   +"           #'<))))";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(true,b);
	}

	@Test
	public void testNoStrings2() {
		String s = "(defun csg-intersection-intersect-all (obj-a obj-b)"
   +"(lambda (ray)"		   
   +" (flet ((inside-p (obj) (lambda (d) (inside-p obj (ray-point ray d)))))"
   +"    (merge 'fvector"
   +"           (remove-if-not (inside-p obj-b) (intersect-all obj-a ray))"
   +"           (remove-if-not (inside-p obj-a) (intersect-all obj-b ray))"
   +"           #'<)))))";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(false,b);
	}

	@Test
	public void testStrings1() {
		String s = "(defparameter *my-string* (string \"(Groucho Marx\"))";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(true,b);
	}

	@Test
	public void testStrings2() {
		String s = "(defparameter *my-string* string \"(Groucho Marx\"))";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(false,b);
	}
	
	@Test
	public void testStringsWithQuotes1() {
		String s = "(defparameter *my-string* (string \"(Groucho \\\"Marx\"))";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(true,b);
	}

	@Test
	public void testStringsWithQuotes2() {
		String s = "(defparameter *my-string* (string \"(Groucho \\\"Marx\")";
		boolean b = Parenthesis.checkLispParenthesis(s);
		assertEquals(false,b);
	}

}
