package com.availity.sgl.lisp;

public class Parenthesis {

	public static boolean checkLispParenthesis(String s) {
		int numberOpen = 0;
		boolean inString = false;
		char ch;
		char prev = ' ';
		
		for (int i=0;i<s.length();i++) {
			ch = s.charAt(i);
			if (inString) {
				if (ch == '"' && prev != '\\') {
					inString = false;
				}
			} else {
				if (ch == '"' && prev != '\\') {
					inString = true;
				}
				if (ch == '(') {
					numberOpen++;
				}
				if (ch == ')') {
					numberOpen--;
				}
			}
			prev = ch;
		}
		
		return numberOpen==0;
	}
}
