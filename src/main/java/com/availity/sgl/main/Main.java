package com.availity.sgl.main;

import com.availity.sgl.lisp.Parenthesis;

public class Main {

	public static void main(String[] args) {
		
		if (args.length ==0 ) {
			System.out.println("Usage:\n");
			System.out.println("java -jar lisp_parenthesis-executable-1.0.jar <string_1> <string_2> ...\n\n");
			System.out.println("This will treat each input string as a LISP code snippet to verify if their");
			System.out.println("parenthesis are balanced or not.\n");
			System.exit(1);
		}
		for (int i=0;i<args.length;i++) {
			if (Parenthesis.checkLispParenthesis(args[i])) {
				System.out.println("lisp code ["+args[i]+"] has balanced parenthesis");	
			} else {
				System.out.println("lisp code ["+args[i]+"] does not have balanced parenthesis");					
			}			
		}

	}

}
