# lisp_parenthesis
###### Sergio Guzman-Lara

### Overview

This is my implementation for the Coding exercise (number 4). For reference here is what I used:

* Spring Tool Suite version 3.8.1.RELEASE, with Buildship plugin for gradle
* java version 1.8.0_211
* gradle (when running from command line) version 5.5.1

### Assumptions

    * Implementation should handle parenthesis in strings
   
### How to run

    * git clone https://gitlab.com/sglguzman/lisp_parenthesis.git
    * cd lisp_parenthesis
    * gradle clean build executableJar
    * java -jar build/libs/lisp_parenthesis-executable-1.0.jar
    
This will print out usage information for the executable jar
